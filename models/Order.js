const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true,"orderId is required"]
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},

	products: [
	{
		productId:{
			type: String,
			required: [true,"productId is required"]
		},
		quantity: {
			type: String,
			default: 1
		}
	}
	]
});

module.exports = mongoose.model("Order",orderSchema);

