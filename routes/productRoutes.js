const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const Order = require("../models/Order");
const auth = require("../auth");


//Route for creating a product (admin only)
router.post("/",auth.verify,(req,res)=>{

	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data);
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))
});

// Route for retrieving all active products
router.get("/active",(req,res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController))
})


// Route for retrieving all products
router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
});


// Route for retrieving single product
router.get("/:productId",(req,res)=>{
	productController.getSingleProduct(req.params).then(resultFromController=>res.send(resultFromController))
})


//Route for updating a product info
// With JWT verification before updating a product info
router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


// Route for archiving a product admin only
router.put("/:productId/archive", auth.verify, (req, res)=> {
	const userAdmin1 = auth.decode(req.headers.authorization).isAdmin;
	if(userAdmin1) {
		productController.archiveProduct(req.params, req.body).then(resultFromController=> res.send(resultFromController));
	} else {
		res.send("Admin User Only!")
	}
});


// Route for activate a product
router.put("/:productId/activate", auth.verify, (req, res)=> {
	const userAdmin2 = auth.decode(req.headers.authorization).isAdmin;
	if(userAdmin2) {
		productController.activateProduct(req.params, req.body).then(resultFromController=> res.send(resultFromController));
	} else {
		res.send("Admin User Only!")
	}
});


// Route for deleting a product
router.delete("/delete/:id",auth.verify,(req,res)=>{
	const admin = auth.decode(req.headers.authorization).isAdmin;
	productController.deleteProduct(req.params.id).then(resultFromController=>res.send(resultFromController));
	
})


module.exports = router;