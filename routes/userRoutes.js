const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


// Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route to check email
router.post("/checkEmail",(req,res)=>{
    userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

// Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});


// Route for update the user as admin
router.put("/updateAdmin/:id",(req,res)=>{
    userController.updateUser(req.params.id).then(resultFromController=>res.send(resultFromController));
});

// Route for update the user as non admin
router.put("/updateUser/:id",(req,res)=>{
    userController.nonAdmin(req.params.id).then(resultFromController=>res.send(resultFromController));
});

// Route for retrieving user details
router.get("/userDetails",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	userController.getUserInfo({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});

// Route for retrieving all user
router.get("/allUser",(req,res)=>{
	userController.getAllUser(req.body).then(resultFromController=>res.send(resultFromController));

});

 

module.exports = router;
