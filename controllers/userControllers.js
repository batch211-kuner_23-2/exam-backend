const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.registerUser = (reqBody) =>{
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	});

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return newUser;
		};
	});
};
 
//Check email
module.exports.checkEmailExists = (reqBody) =>{
    return User.find({email:reqBody.email}).then(result=>{
        if(result.length>0){
            return true;
        }else{
            return false;
        };
    });
};

// User Authentication
module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};




// User update set as admin
module.exports.updateUser = (userId) =>{
    return User.findById(userId).then((result,err)=>{
        if(err){
            console.log(err);
            return err;
        }
        result.isAdmin = true;
        return result.save().then((updatedUser,saveErr)=>{
            if(saveErr){
                console.log(saveErr);
                return saveErr;
            }else{
                return updatedUser;
            }
        })
    })
}

// User update set as non admin
module.exports.nonAdmin = (userId) =>{
    return User.findById(userId).then((result,err)=>{
        if(err){
            console.log(err);
            return err;
        }
        result.isAdmin = false;
        return result.save().then((updatingUser,saveErr)=>{
            if(saveErr){
                console.log(saveErr);
                return saveErr;
            }else{
                return updatingUser;
            }
        })
    })
}


// Retrieve User Details
module.exports.getUserInfo = (data) =>{
	return User.findById(data.userId).then(result=>{
		return result;
	})
}

// Retrieve All User
module.exports.getAllUser = () =>{
	
	return User.find({}).then(result=>{

		return result;
	})
}

